#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_SHAPES_HEIGHTFIELD2_H
#define PBRT_SHAPES_HEIGHTFIELD2_H

// shapes/heightfield.h*
#include "shape.h"

// Heightfield Declarations
class Heightfield2 : public Shape {
public:
	// Heightfield Public Methods
	Heightfield2(const Transform *o2w, const Transform *w2o, bool ro, int nu, int nv, const float *zs);
	~Heightfield2();
	bool CanIntersect() const;
	void Refine(vector<Reference<Shape> > &refined) const;
	BBox ObjectBound() const;

	bool Intersect(const Ray &ray, float *tHit, float *rayEpsilon, DifferentialGeometry *dg) const;
	bool IntersectP(const Ray &ray) const;
	bool RayTriangleIntersection(DifferentialGeometry * dg, float * tHit, 
				const Ray &ray,const Point * P,  int index0, int index1, int index2 , const float * uvs, Vector& z0, Vector& z1) const;
	bool RayTriangleIntersectionP(const Ray &ray,const Point * P,  int index0, int index1, int index2 , const float * uvs) const;
	void GetShadingGeometry(const Transform &obj2world,
			const DifferentialGeometry &dg,
			DifferentialGeometry *dgShading) const;
	void initNormals();
private:
	// Heightfield Data
	float *z;
	Normal * m_pNormal;
	
	Point *P;
	float *uvs;
	int *m_verts ;
	
	float width[2];
	int nx, ny;
	int nVoxels[2];
	int ntris;
	int nverts;
};

Heightfield2 *CreateHeightfield2Shape(const Transform *o2w, const Transform *w2o,
        bool reverseOrientation, const ParamSet &params);
#endif // PBRT_SHAPES_HEIGHTFIELD_H


