#include "stdafx.h"
#include "shapes/heightfield2.h"
#include "shapes/trianglemesh.h"
#include "paramset.h"
#include <iostream>
#include <algorithm>
#include <vector>
#define RAY_EPSILON 1e-3f
void Heightfield2::initNormals()
{
	int x, y;
	int pos = 0;
	for (y = 0; y < ny; ++y) {
		for (x = 0; x < nx; ++x) {
			P[pos].x = uvs[2 * pos] = (float)x / (float)nVoxels[0];
			P[pos].y = uvs[2 * pos + 1] = (float)y / (float)nVoxels[1];
			P[pos].z = z[pos];
			m_pNormal[pos] = Normal(0, 0, 0);
			++pos;
		}
	}
	int * vp = m_verts; 
	int index0, index1, index2;
	Vector p0, p1, p2, p3;
	for (y = 0; y < ny - 1; ++y) {
		for (x = 0; x < nx - 1; ++x) {
	#define VERT(x,y) ((x)+(y)*nx)
			*vp++ = index0 = VERT(x + 1, y);	
			*vp++ = index1 = VERT(x, y);
			*vp++ = index2 = VERT(x + 1, y + 1);
					
			p0 = (Vector) P[index0];
			p1 = (Vector) P[index1];
			p2 = (Vector) P[index2];
			Normal temp = Normalize(Normal(Cross((p1 - p2), (p0 - p2))));
			m_pNormal[index0] += temp;
			m_pNormal[index1] += temp;
			m_pNormal[index2] += temp;

			*vp++ = index0 = VERT(x, y + 1);
			*vp++ = index1 = VERT(x + 1, y + 1);
			*vp++ = index2 = VERT(x, y);
			p0 = (Vector) P[index0];
			p1 = (Vector) P[index1];
			p2 = (Vector) P[index2];
			temp = Normalize(Normal(Cross((p1 - p2), (p0 - p2))));
			m_pNormal[index0] += temp;
			m_pNormal[index1] += temp;
			m_pNormal[index2] += temp;
		}
	#undef VERT
	}
	//For each vertex, normalize the Normal 
	pos = 0;
	for (y = 0; y < ny; ++y) {
		for (x = 0; x < nx; ++x) {
			m_pNormal[pos] = Normalize(m_pNormal[pos]);
			pos++;
		}	
	}	
	return ;
}


// Heightfield Method Definitions
Heightfield2::Heightfield2(const Transform *o2w, const Transform *w2o, 
	bool ro, int x, int y, const float *zs)
	: Shape(o2w, w2o, ro) {
	nx = x;
	ny = y;
	nVoxels[0] = nx - 1;
	nVoxels[1] = ny - 1;
	z = new float[nx * ny];
	m_pNormal = new Normal[nx * ny];
	ntris = 2 * (nx - 1) * (ny - 1);	//number of triangles
	nverts = nx * ny;	//number of grid vertices
	width[0] = 1.0 / (nx - 1);		//max coordinate is 1
	width[1] = 1.0 / (ny - 1); 

	P = new Point[nx * ny];
	uvs = new float[2 * nx * ny];	//Every grid vertex has (u, v)
	m_verts = new int[3 * ntris];	//This is index, verts[0..3*ntris] = [0..nx*ny];
	
	memcpy(z, zs, nx * ny*sizeof(float));
	initNormals();
}

Heightfield2::~Heightfield2() {
	delete[] z;
	delete[] m_pNormal;
	delete [] m_verts;
	delete [] P;
	delete [] uvs;	
}

BBox Heightfield2::ObjectBound() const {
	float minz = z[0], maxz = z[0];
	for (int i = 1; i < nx*ny; ++i) {
		if (z[i] < minz) minz = z[i];
		if (z[i] > maxz) maxz = z[i];
	}
	return BBox(Point(0, 0, minz), Point(1, 1, maxz));
}

bool Heightfield2::CanIntersect() const {
	return true;
}

void Heightfield2::GetShadingGeometry(const Transform &obj2world, const DifferentialGeometry &dg, DifferentialGeometry *dgShading) const {
	// Determine grid number and triangle from uv.
	int gx = floor(dg.u * nVoxels[0]);
	int gy = floor(dg.v * nVoxels[1]);
	if(gx == nVoxels[0])	gx--;
	if(gy == nVoxels[1])	gy--;

	//Get barycentric coordinates.
	float b0, b1, b2;
	Normal zN[3], dndu, dndv;
	if ((dg.u - gx * width[0]) * width[1] < (dg.v - gy * width[1]) * width[0]) {//upper triangle
		b1 = dg.u * nVoxels[0] - (float)gx;
		b2 = (float)(gy + 1) - dg.v * nVoxels[1];
		zN[0] = m_pNormal[(gy + 1) * nx + gx];
		zN[1] = m_pNormal[(gy + 1) * nx + gx + 1];
		zN[2] = m_pNormal[gy * nx + gx];
		dndu = nVoxels[0] * (zN[1] - zN[0]);
		dndv = -nVoxels[1] * (zN[2] - zN[0]);
	} else {//lower triangle
		b1 = (float)(gx + 1) - dg.u * nVoxels[0];
		b2 = dg.v * nVoxels[1] - (float)gy;
		zN[0] = m_pNormal[gy * nx + gx + 1];
		zN[1] = m_pNormal[gy * nx + gx];
		zN[2] = m_pNormal[(gy + 1) * nx + gx + 1];
		dndu = -nVoxels[0] * (zN[1] - zN[0]);
		dndv = nVoxels[1] * (zN[2] - zN[0]);
	}
	b0 = 1. - b1 - b2;
	
	//Get normal and normals and vectors
	Normal ns = Normalize(obj2world(b0 * zN[0] + b1 * zN[1] + b2 * zN[2]));
	Vector ss = Normalize(dg.dpdu), ts = Normalize(Cross(ss, ns));
	ss = Cross(ts, ns);

	//Return shading geometry
	*dgShading = DifferentialGeometry(dg.p, ss, ts, obj2world(dndu), obj2world(dndv), dg.u, dg.v, dg.shape);
	//*dgShading = dg;
	//return ;
}

bool Heightfield2::RayTriangleIntersection(DifferentialGeometry *dg, float *tHit, const Ray &ray,const Point * P,  int index0, int index1, int index2, const float * uvs, Vector& z0, Vector& z1) const {
		const Point &p1 = P[index0];
		const Point &p2 = P[index1];
		const Point &p3 = P[index2];		
		
		Vector e1 = p2 - p1;
		Vector e2 = p3 - p1;
		Vector s1 = Cross(ray.d, e2);
		float divisor = Dot(s1, e1);
		if (divisor == 0.)
			return false;
		float invDivisor = 1.f / divisor;
		// Compute first barycentric coordinate
		Vector d = ray.o - p1;
		float b1 = Dot(d, s1) * invDivisor;
		if (b1 < 0. || b1 > 1.)
			return false;
		// Compute second barycentric coordinate
		Vector s2 = Cross(d, e1);
		float b2 = Dot(ray.d, s2) * invDivisor;
		if (b2 < 0. || b1 + b2 > 1.)
			return false;
		// Compute _t_ to intersection point
		float t = Dot(e2, s2) * invDivisor;
		if (t < ray.mint || t > ray.maxt)
			return false;
		

		// Fill in _DifferentialGeometry_ from triangle hit
		// Compute triangle partial derivatives
		Vector dpdu, dpdv;
		float _uvs[3][2];//just for easy looking
		_uvs[0][0] = uvs[2*index0];
		_uvs[0][1] = uvs[2*index0+1];
		_uvs[1][0] = uvs[2*index1];
		_uvs[1][1] = uvs[2*index1+1];
		_uvs[2][0] = uvs[2*index2];
		_uvs[2][1] = uvs[2*index2+1];

		// Interpolate $(u,v)$ triangle parametric coordinates
		float b0 = 1 - b1 - b2;
		float tu = b0*_uvs[0][0] + b1*_uvs[1][0] + b2*_uvs[2][0];
		float tv = b0*_uvs[0][1] + b1*_uvs[1][1] + b2*_uvs[2][1];
		
		dpdu = z0;
		dpdv = z1;         
                
		Normal dndu = Normal(0,0,0);
		Normal dndv = Normal(0,0,0);
		
		*dg = DifferentialGeometry((*ObjectToWorld)(ray(t)), (*ObjectToWorld)(dpdu), (*ObjectToWorld)(dpdv), (*ObjectToWorld)(dndu), (*ObjectToWorld)(dndv), tu, tv, this);
		
		*tHit = t;
		
		return true;	
}


bool Heightfield2::RayTriangleIntersectionP(const Ray &ray,const Point * P, int index0, int index1, int index2, const float * uvs) const {
		const Point &p1 = P[index0];
		const Point &p2 = P[index1];
		const Point &p3 = P[index2];
		
		Vector e1 = p2 - p1;
		Vector e2 = p3 - p1;
		Vector s1 = Cross(ray.d, e2);
		float divisor = Dot(s1, e1);
		if (divisor == 0.)
			return false;
		float invDivisor = 1.f / divisor;
		// Compute first barycentric coordinate
		Vector d = ray.o - p1;
		float b1 = Dot(d, s1) * invDivisor;
		if (b1 < 0. || b1 > 1.)
			return false;
		// Compute second barycentric coordinate
		Vector s2 = Cross(d, e1);
		float b2 = Dot(ray.d, s2) * invDivisor;
		if (b2 < 0. || b1 + b2 > 1.)
			return false;
		// Compute _t_ to intersection point
		float t = Dot(e2, s2) * invDivisor;
		if (t < ray.mint || t > ray.maxt)
			return false;
	
		return true;	
}

bool Heightfield2::Intersect(const Ray &r, float *tHit, float *rayEpsilon, DifferentialGeometry *dg) const {
	//Transform input ray form world to object space
	Ray ray;
	ray = (*WorldToObject)(r);
	bool IntersectVoxel = false; //Default is no intersection, false
	//compute the entrance and exit parametric t value
	float tx1 = -ray.o.x / ray.d.x;
	float tx2 = (1.f - ray.o.x) / ray.d.x;
	float ty1 = -ray.o.y / ray.d.y;
	float ty2 = (1.f - ray.o.y) / ray.d.y;
	float mint, maxt;
	if (tx1 > tx2)	swap(tx1, tx2);
	if (ty1 > ty2)	swap(ty1, ty2);
	mint = Clamp(max(tx1, ty1), ray.mint, ray.maxt);
	maxt = Clamp(min(tx2, ty2), ray.mint, ray.maxt);
	if (mint > maxt)	return false;
	//2D DDA
	float nextCrossingT[2];
	int pos[2], step[2];//pos is corrent grid NO.
	
	//Initialize traversal info
	for (int axis = 0; axis < 2; ++axis) {
		pos[axis] = Clamp(Float2Int(ray(mint)[axis] * nVoxels[axis]), 0, nVoxels[axis] - 1);
		(ray.d[axis] >= 0.f) ? step[axis] = 1 : step[axis] = -1;
	}
	nextCrossingT[0] = ((pos[0] + ((step[0] + 1) >> 1)) * width[0] - ray.o.x) / ray.d[0];
	nextCrossingT[1] = ((pos[1] + ((step[1] + 1) >> 1)) * width[1] - ray.o.y) / ray.d[1];
	int block_no, tri_no1, tri_no2, index0, index1, index2, stepAxis;//triangle vertices coordinates
	for(;pos[0] < nVoxels[0] && pos[0] > -1 && pos[1] < nVoxels[1] && pos[1] > -1;){
		//Initialize a small bounding box
		//4 vertices are (pos[0], pos[1]), (pos[0], pos[1] + 1), (pos[0] + 1, pos[1]), (pos[0] + 1, pos[1] + 1)
		tx1 = (pos[0] * width[0] - ray.o.x) / ray.d.x;
		tx2 = ((pos[0] + 1) * width[0] - ray.o.x) / ray.d.x;
		ty1 = (pos[1] * width[1] - ray.o.y) / ray.d.y;
		ty2 = ((pos[1] + 1) * width[1] - ray.o.y) / ray.d.y;
		if (tx1 > tx2)	swap(tx1, tx2);
		if (ty1 > ty2)	swap(ty1, ty2);
		float in_mint = Clamp(max(tx1, ty1), ray.mint, ray.maxt);
		float in_maxt = Clamp(min(tx2, ty2), ray.mint, ray.maxt);
		int ver_no = pos[1] * nx + pos[0];
		block_no = pos[1] * nVoxels[0] + pos[0];
		if (in_mint > in_maxt || min(ray(in_mint).z, ray(in_maxt).z) > max(max(max(z[ver_no], z[ver_no + 1]), z[ver_no + nx]), z[ver_no + nx + 1]) || max(ray(in_mint).z, ray(in_maxt).z) < min(min(min(z[ver_no], z[ver_no + 1]), z[ver_no + nx]), z[ver_no + nx + 1])) {;}	
		else {
			Vector z0, z1;
			tri_no1 = 2 * block_no + 0;
			//we need to test two triangles in current voxel
			index0 = m_verts[tri_no1 * 3 + 0];//get the vertex coordinate
			index1 = m_verts[tri_no1 * 3 + 1];
			index2 = m_verts[tri_no1 * 3 + 2];
			z0 = Vector(1, 0, -(P[index1].z - P[index0].z) * (nVoxels[1]));
			z1 = Vector(0, 1, (P[index2].z - P[index0].z) * (nVoxels[0]));
			IntersectVoxel = this -> RayTriangleIntersection(dg, tHit, ray, P, index0, index1, index2, uvs, z0, z1);
			if (IntersectVoxel == true) break;//get intersection!!!

			tri_no2 = 2 * block_no + 1;
			index0 = m_verts[tri_no2 * 3 + 0];//get the vertex coordinate
			index1 = m_verts[tri_no2 * 3 + 1];
			index2 = m_verts[tri_no2 * 3 + 2];
			z0 = Vector(1, 0, (P[index1].z - P[index0].z) * (nVoxels[1]));
			z1 = Vector(0, 1, -(P[index2].z - P[index0].z) * (nVoxels[0]));
			IntersectVoxel = this -> RayTriangleIntersection(dg, tHit, ray, P, index0, index1, index2, uvs, z0, z1);		
			if (IntersectVoxel == true) break;//get intersection!!!
		}
		//advance to next voxel
		if (min(nextCrossingT[0], nextCrossingT[1]) > maxt)	break;
		(nextCrossingT[0] > nextCrossingT[1]) ? (stepAxis = 1) : (stepAxis = 0);
		pos[stepAxis] += step[stepAxis];
		nextCrossingT[0] = ((pos[0] + ((step[0] + 1) >> 1)) * width[0] - ray.o.x) / ray.d[0];
		nextCrossingT[1] = ((pos[1] + ((step[1] + 1) >> 1)) * width[1] - ray.o.y) / ray.d[1];
	}//end for
	if (IntersectVoxel == true)
		*rayEpsilon = *tHit * 1e-3f;
	return IntersectVoxel;
}


bool Heightfield2::IntersectP(const Ray &r) const {
	//Transform input ray form world to object space
	Ray ray;
	ray = (*WorldToObject)(r);
	bool IntersectVoxel = false; //Default is no intersection, false
	//compute the entrance and exit parametric t value
	float tx1 = -ray.o.x / ray.d.x;
	float tx2 = (1.f - ray.o.x) / ray.d.x;
	float ty1 = -ray.o.y / ray.d.y;
	float ty2 = (1.f - ray.o.y) / ray.d.y;
	float mint, maxt;
	if (tx1 > tx2)	swap(tx1, tx2);
	if (ty1 > ty2)	swap(ty1, ty2);
	mint = Clamp(max(tx1, ty1), ray.mint, ray.maxt);
	maxt = Clamp(min(tx2, ty2), ray.mint, ray.maxt);
	if (mint > maxt)	return false;
	//2D DDA
	float nextCrossingT[2];
	int pos[2], step[2];//pos is corrent grid NO.
	
	//Initialize traversal info
	for (int axis = 0; axis < 2; ++axis) {
		pos[axis] = Clamp(Float2Int(ray(mint)[axis] * nVoxels[axis]), 0, nVoxels[axis] - 1);
		(ray.d[axis] >= 0.f) ? step[axis] = 1 : step[axis] = -1;
	}
	nextCrossingT[0] = ((pos[0] + ((step[0] + 1) >> 1)) * width[0] - ray.o.x) / ray.d[0];
	nextCrossingT[1] = ((pos[1] + ((step[1] + 1) >> 1)) * width[1] - ray.o.y) / ray.d[1];
	int block_no, tri_no1, tri_no2, index0, index1, index2, stepAxis;//triangle vertices coordinates
	for(;pos[0] < nVoxels[0] && pos[0] > -1 && pos[1] < nVoxels[1] && pos[1] > -1;){
		//Initialize a small bounding box
		//4 vertices are (pos[0], pos[1]), (pos[0], pos[1] + 1), (pos[0] + 1, pos[1]), (pos[0] + 1, pos[1] + 1)
		tx1 = (pos[0] * width[0] - ray.o.x) / ray.d.x;
		tx2 = ((pos[0] + 1) * width[0] - ray.o.x) / ray.d.x;
		ty1 = (pos[1] * width[1] - ray.o.y) / ray.d.y;
		ty2 = ((pos[1] + 1) * width[1] - ray.o.y) / ray.d.y;
		if (tx1 > tx2)	swap(tx1, tx2);
		if (ty1 > ty2)	swap(ty1, ty2);
		float in_mint = Clamp(max(tx1, ty1), ray.mint, ray.maxt);
		float in_maxt = Clamp(min(tx2, ty2), ray.mint, ray.maxt);
		int ver_no = pos[1] * nx + pos[0];
		block_no = pos[1] * nVoxels[0] + pos[0];
		if (in_mint > in_maxt || min(ray(in_mint).z, ray(in_maxt).z) > max(max(max(z[ver_no], z[ver_no + 1]), z[ver_no + nx]), z[ver_no + nx + 1]) || max(ray(in_mint).z, ray(in_maxt).z) < min(min(min(z[ver_no], z[ver_no + 1]), z[ver_no + nx]), z[ver_no + nx + 1])) {;}	
		else {
			tri_no1 = 2 * block_no + 0;
			//we need to test two triangles in current voxel
			index0 = m_verts[tri_no1 * 3 + 0];//get the vertex coordinate
			index1 = m_verts[tri_no1 * 3 + 1];
			index2 = m_verts[tri_no1 * 3 + 2];
			IntersectVoxel = this -> RayTriangleIntersectionP(ray, P, index0, index1, index2, uvs);
			if (IntersectVoxel == true) break;//get intersection!!!

			tri_no2 = 2 * block_no + 1;
			index0 = m_verts[tri_no2 * 3 + 0];//get the vertex coordinate
			index1 = m_verts[tri_no2 * 3 + 1];
			index2 = m_verts[tri_no2 * 3 + 2];
			IntersectVoxel = this -> RayTriangleIntersectionP(ray, P, index0, index1, index2, uvs);		
			if (IntersectVoxel == true) break;//get intersection!!!
		}
		//advance to next voxel
		if (min(nextCrossingT[0], nextCrossingT[1]) > maxt)	break;
		(nextCrossingT[0] > nextCrossingT[1]) ? (stepAxis = 1) : (stepAxis = 0);
		pos[stepAxis] += step[stepAxis];
		nextCrossingT[0] = ((pos[0] + ((step[0] + 1) >> 1)) * width[0] - ray.o.x) / ray.d[0];
		nextCrossingT[1] = ((pos[1] + ((step[1] + 1) >> 1)) * width[1] - ray.o.y) / ray.d[1];
	}//end for
	return IntersectVoxel;
}


void Heightfield2::Refine(vector<Reference<Shape> > &refined) const {
	int ntris = 2*(nx-1)*(ny-1);
	refined.reserve(ntris);
	int *verts = new int[3*ntris];
	Point *P = new Point[nx*ny];
	float *uvs = new float[2*nx*ny];
	int nverts = nx*ny;
	int x, y;
	// Compute heightfield vertex positions
	int pos = 0;
	for (y = 0; y < ny; ++y) {
		for (x = 0; x < nx; ++x) {
			P[pos].x = uvs[2*pos]   = (float)x / (float)(nx-1);
			P[pos].y = uvs[2*pos+1] = (float)y / (float)(ny-1);
			P[pos].z = z[pos];
			++pos;
		}
	}
	
	// Fill in heightfield vertex offset array
	int *vp = verts;
	for (y = 0; y < ny-1; ++y) {
		for (x = 0; x < nx-1; ++x) {
	#define VERT(x,y) ((x)+(y)*nx)
			*vp++ = VERT(x, y);
			*vp++ = VERT(x+1, y);
			*vp++ = VERT(x+1, y+1);
	
			*vp++ = VERT(x, y);
			*vp++ = VERT(x+1, y+1);
			*vp++ = VERT(x, y+1);
		}
	#undef VERT
	}
	ParamSet paramSet;
	paramSet.AddInt("indices", verts, 3*ntris);
	paramSet.AddFloat("uv", uvs, 2 * nverts);
	paramSet.AddPoint("P", P, nverts);
	
	//refined.push_back(CreateTriangleMeshShape("trianglemesh",
	//		ObjectToWorld, reverseOrientation, paramSet));
	delete[] P;
	delete[] uvs;
	delete[] verts;
}


Heightfield2 *CreateHeightfield2Shape(const Transform *o2w, const Transform *w2o,
        bool reverseOrientation, const ParamSet &params) {
    int nu = params.FindOneInt("nu", -1);
    int nv = params.FindOneInt("nv", -1);
    int nitems;
    const float *Pz = params.FindFloat("Pz", &nitems);
    Assert(nitems == nu*nv);
    Assert(nu != -1 && nv != -1 && Pz != NULL);
    return new Heightfield2(o2w, w2o, reverseOrientation, nu, nv, Pz);
}

