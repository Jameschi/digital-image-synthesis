#include <iostream>
#include "pbrt.h"
#include "light.h"
#include "texture.h"
#include "shape.h"
#include "scene.h"
#include "mipmap.h"
#include "stdafx.h"
#include "imageio.h"
#include "spectrum.h"
#include "paramset.h"
#include "medianCutEnvironmentLight.h"
#include "core/rng.h"
#include "montecarlo.h"
#include "sh.h"
#include "lights/infinite.h"
#include "time.h"

float sat::I(int x, int y) {
	if (x < 0 || y < 0)
		return 0.f;
	return _sat[y * _width + x];
}

float sat::luminance(float r, float g, float b) {
	return r * 0.2125f + g * 0.7154f + b * 0.0721f;
}

void sat::create(RGBSpectrum *texels, int width, int height, int nc) {
	_width = width;
	_height = height;
	_sat.clear();
	_sat.resize(_width * _height);
	float rgb[3], ixy;
	for (int y = 0; y < _height; ++y) {
		for (int x = 0; x < _width;  ++x) {
			int i = y * _width + x;
			texels[i].ToRGB(rgb);
			ixy = luminance(rgb[0], rgb[1] ,rgb[2]);
			_sat[i] = sinf(float(y) / float(height) * M_PI) * ixy + I(x - 1, y) + I(x, y - 1) - I(x - 1, y - 1);
		}
	}
}

float sat::sum(int ax, int ay, int bx, int by, int cx, int cy, int dx, int dy) {
	return I(dx, dy) + I(ax, ay) - I(bx, by) - I(cx, cy);
}

void sat_region::create(int x, int y, int w, int h, sat* sat) {
	x_ = x; 
	y_ = y; 
	w_ = w;
	h_ = h; 
	sat_ = sat;
	sum_ = sat_->sum(x - 1, y - 1, x + w - 1, y - 1, x - 1, y + h - 1, x + w - 1, y + h - 1);
}

void sat_region::split_w(sat_region& A) {
	for (int w = 0; w <= w_; ++w) {
		A.create(x_, y_, w, h_, sat_);
		// if region left has approximately half the energy
		if (A.sum_ * 2.f >= sum_)
			break;
	}

}

/* Split region horizontally into subregions A and B. */
void sat_region::split_w(sat_region& A, sat_region& B) {
	split_w(A);
	B.create(x_ + A.w_, y_, w_ - A.w_, h_, sat_);
}

void sat_region::split_h(sat_region& A) {
	for (int h = 0; h <= h_; ++h) {
		A.create(x_, y_, w_, h, sat_);
		// if region top has approximately half the energy of the entire thing stahp
		if (A.sum_ * 2.f >= sum_)
			break;
	}

}

/* Split region vertically into subregions A and B. */
void sat_region::split_h(sat_region& A, sat_region& B) {
	split_h(A);
	B.create(x_, y_ + A.h_, w_, h_ - A.h_, sat_);
}

vLight sat_region::centroid(Spectrum* texels, int width, int height) {
	float rgb[3] = {0.f, 0.f, 0.f};//the color of light source is the sum of all the pixels in the region
	float tmp[3];
	float total = 0.f;
	float phi = 0.f, theta = 0.f;
	for(int j = y_; j < y_ + h_; ++j) {
		for(int i = x_; i < x_ + w_; ++i) {
			//calculate theta phi
			float intensity = sat_->sum(i, j, i - 1, j, i, j - 1, i - 1, j - 1);
			theta += intensity * i;
			phi += intensity * j;
			//calculate rgb
			texels[j * width + i].ToRGB(tmp);
			for(int k = 0; k < 3; ++k) {
				rgb[k] += ((sinf(float(j) / float(height) * M_PI)) * tmp[k]);
			}
		}
	}
	theta /= sum_;
	phi /= sum_;
	theta = theta / width * M_PI * 2.f;
	phi = phi / height * M_PI;
	Vector dir(cosf(theta) * sinf(phi), sinf(theta) * sinf(phi), cosf(phi));
	for(int i = 0; i < 3; ++i) {
		rgb[i] *= (2.f * M_PI * M_PI) / (width * height);
	}
	Spectrum spectrum;
	spectrum = spectrum.FromRGB(rgb);
	vLight ret = {dir, spectrum};
	return ret;
}

void split_recursive(sat_region& r, int n, std::vector<sat_region>& regions, int height) {
	// check: can't split any further?
	if (r.h_ < 1 || r.w_ < 1 || n == 0) {
		regions.push_back(r);
		return;
	}
	sat_region A, B;
	float tmpw = r.w_ * sinf((r.y_ + (r.h_) * 0.5f) / float(height) * M_PI);
	if (tmpw > r.h_) {
		r.split_w(A, B);
	}
	else {
		r.split_h(A, B);
	}
	split_recursive(A, n - 1, regions, height);
	split_recursive(B, n - 1, regions, height);
}

void create_lights(std::vector<sat_region>& regions, std::vector<vLight>& lights, Spectrum* texels, int width, int height) {
	// set light at centroid
	for (int i = 0; i < regions.size(); ++i) {
		lights.push_back(regions[i].centroid(texels, width, height));
	}
}
int convertpow(int x) {
	int ret = -1;
	while(x > 0) {
		++ret;
		x /= 2;
	}
	return ret;
}
MedianCutEnvironmentLight::MedianCutEnvironmentLight(const Transform &light2world, const Spectrum &L, int ns, const string &texmap)
    : Light(light2world, ns) {
	int width = 0, height = 0;
	// Read texel data from _texmap_ into _texels_
	if (texmap != "") {
		texels = ReadImage(texmap, &width, &height);
		if (texels)
	    	for (int i = 0; i < width * height; ++i) {
			texels[i] *= L.ToRGBSpectrum();
		}
	}
	if (!texels) {
		width = height = 1;
		texels = new RGBSpectrum[1];
		texels[0] = L.ToRGBSpectrum();
	}
	radianceMap = new MIPMap<RGBSpectrum>(width, height, texels);
	int powof2 = convertpow(ns);
	lum_sat.create(texels, width, height, ns);
	r.create(0, 0, width, height, &lum_sat);
	split_recursive(r, powof2, regions, height);
	create_lights(regions, lights, texels, width, height);
}

bool MedianCutEnvironmentLight::IsDeltaLight() const { 
	return true; 
}

Light *CreateMedianCutEnvironmentLight(const Transform &light2world, const ParamSet &paramSet) {
	Spectrum L = paramSet.FindOneSpectrum("L", Spectrum(1.0));
	Spectrum sc = paramSet.FindOneSpectrum("scale", Spectrum(1.0));
	string texmap = paramSet.FindOneFilename("mapname", "");
	int nSamples = paramSet.FindOneInt("nsamples", 1);
	if (PbrtOptions.quickRender) 
		nSamples = max(1, nSamples / 4);
	return new MedianCutEnvironmentLight(light2world, L * sc, nSamples, texmap);
}

Spectrum MedianCutEnvironmentLight::Sample_L(const Point &p, float pEpsilon, const LightSample &ls,
        	float time, Vector *wi, float *pdf, VisibilityTester *visibility) const{
	int i = rand() % lights.size();
	*pdf = 1 / float(nSamples);
	*wi = Normalize(LightToWorld(lights[i].dir));
	visibility->SetRay(p, pEpsilon, *wi, time);
	return lights[i].spectrum;
}

Spectrum MedianCutEnvironmentLight::Le(const RayDifferential &r) const {
    Vector wh = Normalize(WorldToLight(r.d));
    float s = SphericalPhi(wh) * INV_TWOPI;
    float t = SphericalTheta(wh) * INV_PI;
    return Spectrum(radianceMap->Lookup(s, t), SPECTRUM_ILLUMINANT);
}

