#ifndef MEDIANCUTINVIRONMENTLIGHT_H
#define MEDIANCUTINVIRONMENTLIGHT_H

#include <iostream>
#include "pbrt.h"
#include "light.h"
#include "texture.h"
#include "shape.h"
#include "scene.h"
#include "mipmap.h"

// EnvironmentLight Definitions
struct vLight {
	Vector dir;
	Spectrum spectrum;
};

class sat{//summed area table class
public:
	int _width, _height;
	std::vector<float> _sat;
	float I(int x, int y);
	float luminance(float r, float g, float b);
	void create(RGBSpectrum *texels, int width, int height, int nc);
	float sum(int ax, int ay, int bx, int by, int cx, int cy, int dx, int dy);
};

class sat_region {
public:
	int x_, y_, w_, h_;
	float sum_;
	sat* sat_;
	void create(int x, int y, int w, int h, sat* sat);
	void split_w(sat_region& A);
	/* Split region horizontally into subregions A and B. */
	void split_w(sat_region& A, sat_region& B);
	void split_h(sat_region& A);
	/* Split region vertically into subregions A and B. */
	void split_h(sat_region& A, sat_region& B);
	vLight centroid(Spectrum* texels, int width, int height);
};

class MedianCutEnvironmentLight : public Light {
public:
	// EnvironmentLight Public Methods
	MedianCutEnvironmentLight(const Transform &light2world,	const Spectrum &power, int ns,
			  const string &texmap);
	bool IsDeltaLight() const;
        Spectrum Power(const Scene *) const{}
	Spectrum Le(const RayDifferential &r) const;
	float Pdf(const Point &, const Vector &) const{}
	Spectrum Sample_L(const Point &p, float pEpsilon, const LightSample &ls,
        	float time, Vector *wi, float *pdf, VisibilityTester *visibility) const;
	Spectrum Sample_L(const Scene *scene, const LightSample &ls, float u1, float u2,
        	float time, Ray *ray, Normal *Ns, float *pdf) const{}

private:
	sat lum_sat;
	std::vector<sat_region> regions;
	sat_region r;
	std::vector<vLight> lights;
	Spectrum* texels;
	MIPMap<RGBSpectrum> *radianceMap;
};

Light *CreateMedianCutEnvironmentLight(const Transform &light2world,
		const ParamSet &paramSet);
#endif
