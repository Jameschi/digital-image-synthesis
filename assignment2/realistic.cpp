﻿#include "stdafx.h"
#include "cameras/realistic.h"
#include "paramset.h"
#include "sampler.h"
#include "montecarlo.h"
#include <iostream>
using namespace std;

RealisticCamera *CreateRealisticCamera(const ParamSet &params,//copy from TA's code
					  const AnimatedTransform &cam2world, Film *film) {
	// Extract common camera parameters from \use{ParamSet}
	float hither = params.FindOneFloat("hither", -1);
	float yon = params.FindOneFloat("yon", -1);
	float shutteropen = params.FindOneFloat("shutteropen", -1);
	float shutterclose = params.FindOneFloat("shutterclose", -1);

	// Realistic camera-specific parameters
	string specfile = params.FindOneString("specfile", "");
	float filmdistance = params.FindOneFloat("filmdistance", 70.0); 
 	float fstop = params.FindOneFloat("aperture_diameter", 1.0);	
	float filmdiag = params.FindOneFloat("filmdiag", 35.0);

	Assert(hither != -1 && yon != -1 && shutteropen != -1 
			&& shutterclose != -1 && filmdistance!= -1);
	if (specfile == "")
		Severe("No lens spec file supplied!\n");
	
	return new RealisticCamera(cam2world, hither, yon, shutteropen, shutterclose,
				   filmdistance, fstop, specfile, filmdiag, film);
}

float load(const string& specfile, vector<lensSurface>& lens) {//This function parse the lens dat file and return the z of exit pupil
	FILE* fin = fopen(specfile.c_str(), "r");
	//parameters
	float lensR, z_Intercept, ndBack, ap;
	lensSurface tmp_lens;
	float z = 0.f;
	float ndFront = 1.f;
	//In my data structure, the front lens is at z = 0, film is at the most negative z position
	char buffer[1024];
	while(fgets(buffer, sizeof(buffer), fin)) {
		if (buffer[0] == '#') {
			continue;
		}
		sscanf(buffer, "%f%f%f%f", &lensR, &z_Intercept, &ndBack, &ap);
		if (lensR == 0.f)//aperture
			ndBack = ndFront;
		//update refraction surface set
		tmp_lens.o = Point(0.f, 0.f, z - lensR);
		tmp_lens.radius = lensR;
		tmp_lens.ndBack = ndBack;
		tmp_lens.ndFront = ndFront;
		tmp_lens.ndRatio = tmp_lens.ndBack / tmp_lens.ndFront;
		tmp_lens.ndRatioSquare = tmp_lens.ndRatio * tmp_lens.ndRatio;
		tmp_lens.apR = ap * 0.5;
		lens.push_back(tmp_lens);
		z -= z_Intercept;
		ndFront = ndBack;
	}
	fclose(fin);
	return z;
}

RealisticCamera::RealisticCamera(const AnimatedTransform &cam2world, float hither, float yon,
                                 float sopen, float sclose, float filmdistance, float aperture_diameter,
                                 const string &specfile, float filmdiag, Film *f)
                                 : Camera(cam2world, sopen, sclose, f), ShutterOpen(sopen),
				ShutterClose(sclose), film(f) {
	float z = load(specfile, lens);
	
	// Film
	filmDist = filmdistance;
	filmZ = z - filmDist;
	filmDiag = filmdiag;
	rasterDiag = sqrtf(film->xResolution * film->xResolution
		+ film->yResolution * film->yResolution);

	// Sample disk at the exit pupil and enlarge to ensure sampling
	lensSurface exit_pupil = lens.back();
	sampleDiskR = exit_pupil.apR;
	sampleDiskZ = exit_pupil.o.z + exit_pupil.radius;
	sampleDiskA = sampleDiskR * sampleDiskR * M_PI;
}

bool Refract(Ray * r, const lensSurface& s) {//Ray is normalized
	float t_hit;
	Point point_hit;
	// Aperture
	if (s.radius == 0) {
		t_hit = (s.o.z - r->o.z) / (r->d.z);
	}
	else {
		//move it to origin, easy for intersect calculation
		Ray ray = *r;
		ray.o.z -= s.o.z - 0.f;
		//(ray.o.x + t * ray.d.x) ^ 2 + (ray.o.y + t * ray.d.y) ^ 2 + (ray.o.z + t * ray.d.z) ^ 2 = s.radius ^ 2
		//doing some math then we get the coefficient:
		float A = ray.d.x * ray.d.x + ray.d.y * ray.d.y + ray.d.z * ray.d.z;
		float B = 2.f * (ray.d.x * ray.o.x + ray.d.y * ray.o.y + ray.d.z * ray.o.z);
		float C = ray.o.x * ray.o.x + ray.o.y * ray.o.y + ray.o.z * ray.o.z - s.radius * s.radius;

		// Solve quadratic equation for t values
		float t_front, t_back;
		if (!Quadratic(A, B, C, &t_front, &t_back))
			return false;

		// Compute intersection distance along ray
		// If radius > 0, back ray intersect
		if (s.radius <= 0.f)//lens is concave
			t_hit = t_front;
		else //lens is convex
			t_hit = t_back;		
		if (t_hit > ray.maxt || t_hit < ray.mint)
			return false;
	}

	// compute hit position and test whether inside
	point_hit = (*r)(t_hit);	
	if (point_hit.x * point_hit.x + point_hit.y * point_hit.y - s.apR * s.apR > 0)
		return false;//outside the circle, ray is blocked
	
	// Normal pointing to the front
	Vector n;
	if (s.radius == 0.f) { // aperture
		n = Vector(0.f, 0.f, -1.f);
	}
	else {
		n = Normalize(s.o - point_hit);
		if (s.radius < 0.f) // pointing to the front
			n = -n;
	}
	//SNELL Heckber's method
	float c1 = -Dot(r->d, n);
	float c2 = sqrtf(1.f - s.ndRatioSquare * (1.f - c1 * c1));
	*r = Ray(point_hit, Normalize(s.ndRatio * r->d + (s.ndRatio * c1 - c2) * n), 0, INFINITY);
	return true;
}

float RealisticCamera::GenerateRay(const CameraSample &sample, Ray *ray) const {
	// Point on film rasterToCamera transform
	float filmX = -(sample.imageX - film->xResolution * 0.5) * filmDiag / rasterDiag;
	float filmY = (sample.imageY - film->yResolution * 0.5) * filmDiag / rasterDiag;

	float sampleDiskU, sampleDiskV;
	ConcentricSampleDisk(sample.lensU, sample.lensV, &sampleDiskU, &sampleDiskV);//0<=sampleDiskUV<=1
	sampleDiskU *= sampleDiskR;//enlarge to whole exit pupil
	sampleDiskV *= sampleDiskR;

	Point Pfilm(filmX, filmY, filmZ);//point on film
	Point Plens(sampleDiskU, sampleDiskV, sampleDiskZ);//exit pupil
	*ray = Ray(Pfilm, Normalize(Plens - Pfilm), 0.f, INFINITY);
	
	float cos = Dot(ray->d, Vector(0.f, 0.f, 1.f));
	float weight = cos * cos * cos * cos / DistanceSquared(Pfilm, Plens) * sampleDiskA;

	vector<lensSurface>::const_reverse_iterator rit;
	for (rit = lens.rbegin(); rit != lens.rend(); rit++) {
		if (Refract(ray, *rit) == false)//ray is normalized
			return 0.f;
	}

	CameraToWorld (*ray, ray);
	ray->d = Normalize(ray->d);

	return weight;
}
