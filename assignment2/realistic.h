#ifndef PBRT_CAMERAS_REALISTIC_H
#define PBRT_CAMERAS_REALISTIC_H

#include "pbrt.h"
#include "camera.h"
#include "film.h"
#include "shapes/sphere.h"

class lensSurface {
public:
	Point o;        // Spherical center
	float radius;   // Spherical radius
	float ndBack;   // Refraction index of back material
	float ndFront;  // Refraction index of front material
	float ndRatio;  // ndBack / ndFront
	float ndRatioSquare; // ndRatio * ndRatio
	float apR;      // Radius of lens
};

class RealisticCamera : public Camera {
public:
	RealisticCamera(const AnimatedTransform &cam2world,
		float hither, float yon, float sopen,
		float sclose, float filmdistance, float aperture_diameter,
		const string &specfile, float filmdiag, Film *film);
	float GenerateRay(const CameraSample &sample, Ray *) const;

private:
	float ShutterOpen;
	float ShutterClose;
	Film * film;
	
	// reference surfaces
	vector<lensSurface> lens;

	float filmZ; // film z position
	float filmDist; // distance between film and exit pupil
	float filmDiag; // diagonal of film actual size
	float rasterDiag; // diagonal of raster space
	float sampleDiskZ; // z position
	float sampleDiskR; // radius
	float sampleDiskA; // area
};
	  
RealisticCamera *CreateRealisticCamera(const ParamSet &params,
        const AnimatedTransform &cam2world, Film *film);

#endif
