#include <stdio.h>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <assert.h>
using namespace std;
int main(int argc, char *argv[])
{
	int meshsize, frames;
	FILE *input, *output;
	char inputfilename[20], outputfilename[20];
	float *x, *y, *z, *foam;
	float threshold;
	printf("Input number of frames ");
	scanf("%d", &frames);
	printf("Input meshsize ");
	scanf("%d", &meshsize);
	printf("Input threshold ");
	scanf("%f", &threshold);
	x = new float[meshsize*meshsize];
	y = new float[meshsize*meshsize];
	z = new float[meshsize*meshsize];
	foam = new float[meshsize*meshsize];
	bzero(foam, (meshsize*meshsize*sizeof(float)));
	for(int i = 1; i <= frames; ++i) {
		//Get inputs
		sprintf(inputfilename, "%s/X%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		input = fopen(inputfilename, "r");
		assert(input != NULL);
		for(int j = 0; j < meshsize*meshsize; ++j){
			fscanf(input, "%f", &x[j]);
		}
		fclose(input);
		sprintf(inputfilename, "%s/Y%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		input = fopen(inputfilename, "r");
		assert(input != NULL);
		for(int j = 0; j < meshsize*meshsize; ++j){
			fscanf(input, "%f", &y[j]);
		}
		fclose(input);
		sprintf(inputfilename, "%s/Z%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		assert(input != NULL);
		input = fopen(inputfilename, "r");
		for(int j = 0; j < meshsize*meshsize; ++j){
			fscanf(input, "%f", &z[j]);
		}
		fclose(input);

		//Calculating foam;
		printf("Analyzing frame %d\n", i);
		for(int j = 0; j < meshsize*meshsize; ++j){
			int right = j + 1, left = j - 1,
				up = j + meshsize, down = j - meshsize;
			if(right%meshsize == 0){
				right -= meshsize;
			}
			if(left == -1 || left % meshsize == meshsize - 1){
				left += meshsize;
			}
			if(up >= meshsize*meshsize){
				up %= meshsize;
			}
			if(down < 0){
				down += meshsize*meshsize;
			}
			float dx1 = (z[j] - z[left])/(x[j] - x[left]);
			float dx2 = (z[right] - z[j])/(x[right] - x[j]);
			float dy1 = (z[j] - z[down])/(x[j] - x[down]);
			float dy2 = (z[up] - z[j])/(x[up] - x[j]);
			if(fabs(dx1 - dx2) >= threshold || fabs(dy1 - dy2) >= threshold){
				foam[j] = min(foam[j] + 0.5, 1.);
			} else {
				foam[j] *= 0.5;
			}
		}
		sprintf(outputfilename, "foammap/foam%05d.ppm", i);
		output = fopen(outputfilename, "wb");
		fprintf(output, "P6\n%d %d\n255\n", meshsize, meshsize);
		for(int j = 0; j < meshsize*meshsize; j++){
			char foamint = (char)(foam[j]*255);
			fwrite(&foamint, sizeof(char), 1, output);
			fwrite(&foamint, sizeof(char), 1, output);
			fwrite(&foamint, sizeof(char), 1, output);
		}
		fclose(output);
	}
	delete[] x;
	delete[] y;
	delete[] z;
	delete[] foam;
}
