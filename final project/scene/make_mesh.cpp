 
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <assert.h>
using namespace std;
struct Vector{
	float x; float y; float z;
	Vector(float a, float b, float c):x(a), y(b), z(c) {}
	Vector operator+(const Vector &v){return Vector(x + v.x, y + v.y, z + v.z);}
};
Vector normalize(const Vector &v){
	float oneOverl = 1./sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
	return Vector(v.x*oneOverl, v.y*oneOverl, v.z*oneOverl);
}
Vector cross(const Vector &v, const Vector &w){
	return Vector(v.y*w.z - v.z*w.y, v.z*w.x - v.x*w.z, v.x*w.y - v.y*w.x);
}
int main(int argc, char *argv[])
{
	int framestart, frameend;
	int meshsize, patchsize;
	FILE *input, *output;
	char inputfilename[20], outputfilename[20];
	float *x, *y, *z;
	float threshold;
	bool toPBRT;
	printf("Input range of frames ");
	scanf("%d%d", &framestart, &frameend);
	printf("Input meshsize ");
	scanf("%d", &meshsize);
	printf("Input patchsize ");
	scanf("%d", &patchsize);
	printf("0) obj 1) pbrt ");
	scanf("%d", &toPBRT);
	x = new float[(meshsize+1)*(meshsize+1)];
	y = new float[(meshsize+1)*(meshsize+1)];
	z = new float[(meshsize+1)*(meshsize+1)];
	for(int i = framestart; i <= frameend; ++i) {
		//Get inputs
		sprintf(inputfilename, "%s/X%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		input = fopen(inputfilename, "r");
		assert(input != NULL);
		for(int j = 0; j < (meshsize+1)*meshsize; ++j){
			if(j%(meshsize+1) == meshsize){
				x[j] = x[j - meshsize] + patchsize;
			}
			else fscanf(input, "%f", &x[j]);
		}
		fclose(input);
		sprintf(inputfilename, "%s/Y%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		input = fopen(inputfilename, "r");
		assert(input != NULL);
		for(int j = 0; j < (meshsize+1)*meshsize; ++j){
			if(j%(meshsize+1) == meshsize){
				y[j] = y[j - meshsize];
			}
			else fscanf(input, "%f", &y[j]);
		}
		fclose(input);
		sprintf(inputfilename, "%s/Z%d.txt", argv[1], i);
		printf("Opening %s...\n", inputfilename);
		assert(input != NULL);
		input = fopen(inputfilename, "r");
		for(int j = 0; j < (meshsize+1)*meshsize; ++j){
			if(j%(meshsize+1) == meshsize){
				z[j] = z[j - meshsize];
			}
			else fscanf(input, "%f", &z[j]);
		}
		fclose(input);
		for(int j = (meshsize + 1)*meshsize; j < (meshsize + 1)*(meshsize + 1); j++){
			x[j] = x[j - (meshsize + 1)*meshsize];
			y[j] = y[j - (meshsize + 1)*meshsize] + patchsize;
			z[j] = z[j - (meshsize + 1)*meshsize];

		}

		++meshsize;

		//Calculating vertices;
		printf("Outputing frame %d as %s\n", i, toPBRT?".pbrt":".obj");
		sprintf(outputfilename, "seamesh/mesh%05d.%s", i, toPBRT?"pbrt":"obj");
		output = fopen(outputfilename, "w");
		
		
		if(toPBRT)fprintf(output, "Shape \"trianglemesh\"  \"point P\" [\n");
		else fprintf(output, "g waves\n");
		for(int j = 0; j < meshsize*meshsize; ++j){
			if(toPBRT){
				fprintf(output, "%f %f %f  ",x[j], y[j], z[j]);
				if(j%meshsize == meshsize - 1)
					fprintf(output, "\n");
			} else {
				fprintf(output, "v %f %f %f\n",x[j], y[j], z[j]);
			}
		}
		
		if(toPBRT) fprintf(output, "] \"normal N\" [");
		for(int j = 0; j < meshsize*meshsize; ++j){
			int right = j + 1, left = j - 1,
				up = j + meshsize, down = j - meshsize;
			if(right%meshsize == 0){
				right -= (meshsize - 1);
			}
			if(left == -1 || left % meshsize == meshsize - 1){
				left += (meshsize - 1);
			}
			if(up >= meshsize*meshsize){
				up %= meshsize;
				up += meshsize;
			}
			if(down < 0){
				down += (meshsize*meshsize + meshsize);
			}
			Vector dirRight(x[right] - x[j], y[right] - y[j], z[right] - z[j]);
			Vector dirUp(x[up] - x[j], y[up] - y[j], z[up] - z[j]);
			Vector dirLeft(x[left] - x[j], y[left] - y[j], z[left] - z[j]);
			Vector dirDown(x[down] - x[j], y[down] - y[j], z[down] - z[j]);
			if(right != j + 1)       {dirRight.x += patchsize;}
			if(up != j + meshsize)   {dirUp.y += patchsize;}
			if(left != j - 1)        {dirLeft.x -= patchsize;}
			if(down != j - meshsize) {dirDown.y -= patchsize;}
			Vector nRU = normalize(cross(dirRight, dirUp));
			Vector nUL = normalize(cross(dirUp, dirLeft));
			Vector nLD = normalize(cross(dirLeft, dirDown));
			Vector nDR = normalize(cross(dirDown, dirRight));
			Vector normal = normalize(nRU + nUL + nLD + nDR);
			//printf("%f %f %f %f  ", nRU.z, nUL.z, nLD.z, nDR.z);
			//printf("%f %f %f %f  ", dirRight.x, dirUp.x, dirLeft.x, dirDown.x);
			if(toPBRT) {
				fprintf(output, "%f %f %f  ", normal.x, normal.y, normal.z);
				if(j % meshsize == meshsize - 1)fprintf(output, "\n");
			} else {
				fprintf(output, "vt %f %f %f\n", normal.x, normal.y, normal.z);
			}
			//if(j % meshsize == meshsize - 1)printf("\n");
		}

		if(toPBRT) fprintf(output, "] \"integer indices\" [\n");
		int currentSquare = 0;
		for(int j = 0; j < meshsize-1; ++j) {
			for(int k = 0; k < meshsize - 1; ++k, ++currentSquare) {
				if(toPBRT){
					fprintf(output, "%d %d %d  %d %d %d  ",
						currentSquare, currentSquare + 1, currentSquare + 1 + meshsize,
						currentSquare, currentSquare + meshsize, currentSquare + 1 + meshsize);
				} else {
					fprintf(output, "f %d//%d %d//%d %d//%d %d//%d\n",
						currentSquare, currentSquare,
						currentSquare + 1, currentSquare + 1,
						currentSquare + 1 + meshsize, currentSquare + 1 + meshsize,
						currentSquare + meshsize, currentSquare + meshsize);
				}
			}
			fprintf(output, "\n");
			++currentSquare;
		}

		if(toPBRT){
			fprintf(output, "] \"float uv\" [\n");
			for(int j = 0; j < meshsize; ++j){
				for(int k = 0; k < meshsize; ++k){
					float u = (float)k / (float)meshsize;
					float v = (float)j / (float)meshsize;
					if(toPBRT) fprintf(output, "%f %f  ", u, v);
					else fprintf(output, "vt %f %f\n", u, v);
				}
				fprintf(output, "\n");
			}
			//Shading normals;
			fprintf(output, "]\n");
		}

		fclose(output);
	}
	delete[] x;
	delete[] y;
	delete[] z;
}
