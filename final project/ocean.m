function [X,Y,Z] = ocean()
    rng(10); % setting seed for random numbers
    
    meshSize = 1024; % field size
    patchSize = 200;
    windDir = [0.1,1];
    A = 1e-5;
    g = 9.81;
    windSpeed = 30;
    lambda = -1.8;
    
    x1 = linspace(-patchSize/2, patchSize/2, meshSize+1); %devide meshSize intervals evenly
    x = x1(1:meshSize);
    y1 = linspace(-patchSize/2, patchSize/2, meshSize+1);
    y = y1(1:meshSize);
    [X,Y] = meshgrid(x,y); % wave field

    i = 1:meshSize; j = 1:meshSize; % indecies
    [I,J] = meshgrid(i,j); % field of indecies
    Kx = 2.0 * pi / patchSize * (I - meshSize / 2.0 + 0.01); % = 2*pi*n / Lx, -N/2 <= n < N/2
    Ky = 2.0 * pi / patchSize * (J - meshSize / 2.0 + 0.01); % = 2*pi*m / Ly, -M/2 <= m < M/2
    K = sqrt(Kx.^2 + Ky.^2);
    W = sqrt(K .* g); %eq 14

    for i = 1:meshSize
        for j = 1:meshSize
            P(i,j) = phillips(Kx(i,j), Ky(i,j), windDir, windSpeed, A, g); 
        end
    end

    H0 = 1/sqrt(2) .* (randn(size(X)) + 1i .* randn(size(X))) .* sqrt(P); % eq25
    
    timeStep = 0.25;
    for t = 1:10
        Ht = H0 .* exp(1i .* W .* (t * timeStep)) + conj(flip(flip(H0,1),2)) .* exp(-1i .* W .* (t * timeStep));
        Hx = Ht;
        Hy = Ht;
        %Now calculate D (choppy wave)
        for i = 1:meshSize
            for j = 1:meshSize
                Hx(i,j) = Ht(i,j) * -1i * Kx(i,j) / K(i,j);
                Hy(i,j) = Ht(i,j) * -1i * Ky(i,j) / K(i,j);
            end
        end
        X1 = X ;%+ lambda * meshSize^2 * real(sign(ifft2(Hx),meshSize));
        Y1 = Y ;%+ lambda * meshSize^2 * real(sign(ifft2(Hy),meshSize));
        Z = (meshSize)^2 * real(sign(ifft2(Ht),meshSize));
        writeData(X1,Y1,Z,t,meshSize);
        rotate3d on;
        surf(X1,Y1,Z); %plot surface
        axis([-patchSize/2 patchSize/2 -patchSize/2 patchSize/2 -40 40]);
        pause(0.02);
    end
end
function writeData(X,Y,Z,t,meshSize)
        prefix = 'C:\Users\JamesChi\Desktop\Data\';
        ext = '.txt';
        for i = 1:3
            path = strcat(prefix,char(i-1+'X'),int2str(t),ext);
            fd = fopen(path, 'w'); 
            for row = 1:meshSize
                for col=1:meshSize
                    if i == 1;
                        fprintf(fd, '%f ', X(row,col));
                    end
                    if i == 2;
                        fprintf(fd, '%f ', Y(row,col));
                    end
                    if i == 3;
                        fprintf(fd, '%f ', Z(row,col));
                    end
                end
            end
            fclose(fd);
        end
end

function P = phillips(kx, ky, windDir, windSpeed, A, g)
    k_sq = kx^2 + ky^2;
    if k_sq == 0
        P = 0;
    else
        L = windSpeed^2 / g;
        k = [kx, ky] / sqrt(k_sq);
        wk = k(1) * windDir(1) + k(2) * windDir(2);
        P = A / k_sq^2 * exp(-1.0 / (k_sq * L^2)) * wk^2; %eq23
        if wk < 0
            P = 0;
        end
    end
end

function H = sign(H1, meshSize)
    H = H1;
    for i = 1:meshSize
        for j = 1:meshSize
            if mod(i+j,2) == 0
                sign = -1;
            else
                sign = 1;
            end
            H(i,j) = H1(i,j) * sign;
        end
    end
end